import 'package:flutter/material.dart';
import 'package:flutter_quiz/quiz_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Quiz',
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Kuis Sederhana'),
      ),
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(20),
          children: [
            Center(
              child: Image.asset(
                'images/logo.jpg',
                width: 300,
                height: 120,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: 40),
            RaisedButton(
              color: Colors.pinkAccent,
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => QuizPage(),
                ),
              ),
              child: Text(
                'MULAI',
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
