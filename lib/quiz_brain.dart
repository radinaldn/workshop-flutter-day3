import 'question.dart';

int _currentNumber = 0;

class QuizBrain {
  List<Question> _questions = [
    Question(
      questionText: "Flutter adalah framework buatan Google",
      questionAnswer: true,
    ),
    Question(
      questionText: "Flutter ditulis menggunakan bahasa pemrograman Dart",
      questionAnswer: true,
    ),
    Question(
        questionText:
            "Flutter hanya dapat digunakan untuk membangun aplikasi Android",
        questionAnswer: false)
  ];

  // fungsi untuk berpindah ke soal berikutnya
  void nextQuestion() {
    if (_currentNumber < _questions.length - 1) {
      _currentNumber++;
    }
  }

  // fungsi untuk mendapatkan soal/pernyataan sesuai indeks nomor saat ini
  String getQuestionText() {
    return _questions[_currentNumber].questionText;
  }

  // fungsi untuk mendapatkan kunci jawaban sesuai indeks nomor saat ini
  bool getCorrectAnswer() {
    return _questions[_currentNumber].questionAnswer;
  }

  // fungsi untuk mendapatkan jumlah soal/pernyataan
  int getQuestionSize() {
    return _questions.length;
  }

  // fungsi untuk mendapatkan indeks nomor soal saat ini
  int getCurrentNumber() {
    return _currentNumber;
  }

  // fungsi untuk mereset indeks nomor soal
  void reset() {
    _currentNumber = 0;
  }
}
